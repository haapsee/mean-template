FROM node:16-alpine as npmi
WORKDIR /app
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install --include=dev

FROM node:16-alpine as development
RUN apk add chromium
RUN export CHROME_BIN=/usr/bin/chromium-browser
WORKDIR /app
COPY --from=npmi /app /app
RUN mkdir /home/node/public
RUN chown node:node /home/node/public
CMD ["npm", "run", "start"]
